# add gitlab group members

Adding members from one gitlab group to another

# Development usage

You need Python above version 3.5 and pip installed  

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/add-gitlab-group-members.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements `pip install -r requirements.txt`
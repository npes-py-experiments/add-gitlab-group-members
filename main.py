from functions import *

# replace placeholder group id's with real group id's
group_1_id = '1234567'
group_2_id = '1234567'

# read token and connect to gitlab
token = get_token('token.txt')
gitlab_connect(token)

# Get the members from the 2 groups
group_1_members = get_group_members(group_1_id)
group_2_members = get_group_members(group_2_id)

# remove members from list that are already in programming project
unique_users = remove_member_duplicates(group_1_members, group_2_members)

# show count of group_2 members before adding
print(f'group id {group_2_id} members before adding: {len(group_2_members)} users')
# count members to be added to group_2
print(f'not in group id {group_2_id}: {len(unique_users)} users')

# add members to group_2
group_2 = get_group(group_2_id)
for member in unique_users:
    print(member)
    group_2.members.create({'user_id': member.id, 'access_level': gitlab.GUEST_ACCESS})

# count members of group_2 after adding
print(f'group id {group_2_id} members after adding {len(count_group_members(group_2_id))} users')

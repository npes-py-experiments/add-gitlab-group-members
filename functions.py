import gitlab
import sys


# open and format token
def get_token(filename='token.txt', out=sys.stdout):
    out.write('using token path {}\n'.format(filename))
    with open(filename) as f:
        token = f.readline()
    out.write("- read token with {} chars\n".format(len(token.strip())))
    return token.strip()


def get_group(group_id):
    group = gl.groups.get(group_id)
    return group


def get_group_members(group_id):
    group = get_group(group_id)
    group_members = group.members.list()
    return group_members


def remove_member_duplicates(group1_members, group2_members):
    unique_members = set(group1_members) - set(group2_members)
    unique_members = list(unique_members)
    return unique_members


def count_group_members(group_id):
    members = get_group_members(group_id)
    no_of_members = len(members)
    return no_of_members


def gitlab_connect(token):
    # try connecting to gitlab's api
    try:
        gl = gitlab.Gitlab('https://gitlab.com/', private_token=token, per_page=100)
        gl.auth()

    except gitlab.exceptions.GitlabError as e:
        print("Error: {}".format(e))
        print("404 could mean bad token...")
        exit(1)

    sys.stdout.write("Authenticated to gitlab\n")
    return "connect ok - authenticated to gitlab"
